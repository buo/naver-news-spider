# encoding: utf-8

import spider
import unittest

class TestSpider(unittest.TestCase):
  # It should not extract the caption of an image.
  # http://m.news.naver.com/read.nhn?oid=011&aid=0002814666&sid1=105&mode=LSD
  def test_get_summary1(self):
    expected = ''.join([
      u'우주비행사 마크 켈리는 우주에서 많은 시간을 보냈다. 국제우주정거장(ISS)에서의 단기 임무를 4'
      u'차례나 수행했으며 지금은 지상에서 쌍둥이 동생이자 우주비행사인 스코트 켈리와 함께 미 항공우주'
      u'국(NASA)의 우주 임무를 수행하고 있다. 스코트는 ISS에 머문 지 만 1년이 돼 간다. 미국 우주'
      u'비행사 중 최장 체류기록이다. 올해 동생이 귀환하면 과학자들은 두 형제의 DNA와 RNA를 포함한 '
      u'모든 신체적 차이를 면밀히 조사할 예정이다. 화성 등으로의 장기 유인탐사에 앞서 우주생활이 인체'
      u'에 미치는 영향을 파악하기 위함이다.'
    ])
    result = spider.get_summary('/read.nhn?oid=011&aid=0002814666&sid1=105&mode=LSD')
    self.assertEqual(result, expected)

  # It should not extract the names of journalist, photographer, and editor.
  # http://m.news.naver.com/read.nhn?oid=047&aid=0002112173
  def test_get_summary2(self):
    expected = ''.join([
      u'전국적 관심을 모으고 있는 대구 수성구갑 국회의원 선거에서 현수막 베껴걸기 논란이 일어 났습니다'
      u'. 이 사건에 대해서는 <오마이뉴스>가'
    ])
    result = spider.get_summary('/read.nhn?oid=047&aid=0002112173')
    self.assertEqual(result, expected)

  # It should recursively remove some parenthesized rubbish.
  # http://m.news.naver.com/read.nhn?oid=008&aid=0003659764
  def test_get_summary3(self):
    expected = ''.join([
      u'중국 시중 은행의 부실채권이 빠르게 늘며 부실채권 비율이 2%를 돌파했다. 2008년 금융위기 이후'
      u' 이후 처음이다. 중국 정부는 부실채권 출자전환과 자산유동화증권(ABS)으로 급한 불을 끄겠다는 '
      u'방침이지만 효과는 장담할 수 없다. 대출이 워낙 급증하며 잠재적 부실채권도 늘고 있어 근본 대책'
      u'이 아니라는 지적이 나온다.'
    ])
    result = spider.get_summary('/read.nhn?oid=008&aid=0003659764')
    self.assertEqual(result, expected)

  # It should remove `【REGION=JOURNAL】 REPORTER = ` from the summary.
  # http://m.news.naver.com/read.nhn?oid=003&aid=0007146400
  def test_get_summary4(self):
    expected = u'한국신용평가는 5일 올해 건설업계를 둘러싼 환경이 적잖이 어려울 것으로 내다봤다.'
    result = spider.get_summary('/read.nhn?oid=003&aid=0007146400')
    self.assertEqual(result, expected)

  # It should remove `(REGION=JOURNAL) REPORTER = ` from the summary.
  # http://m.news.naver.com/read.nhn?oid=001&aid=0008308911
  def test_get_summary5(self):
    expected = ''.join([
      u'러시아 당국이 시베리아 지역에서 불법으로 노동활동을 해오던 북한 근로자 14명을 적발해 본국으로'
      u' 강제 출국시켰다고 현지 언론이 4일(현지시간) 보도했다.'
    ])
    result = spider.get_summary('/read.nhn?oid=001&aid=0008308911')
    self.assertEqual(result, expected)

if __name__ == '__main__':
  unittest.main()
