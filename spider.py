#!/usr/bin/env python
# encoding: utf-8

from scrapy import Selector
import re
import requests

def parse(url, params={}):
  if url.startswith('/'):
    url = ''.join(['http://m.news.naver.com', url])
  resp = requests.get(url, params=params)
  doc = Selector(text=resp.text, type='html')
  return doc

def get_categories():
  return [
    {'id': '100', 'name': 'politics'},
    {'id': '101', 'name': 'economy'},
    {'id': '102', 'name': 'korea'},
    {'id': '105', 'name': 'tech'},
    {'id': '103', 'name': 'life'},
    {'id': '104', 'name': 'world'}
  ]

def get_items(category_id):
  doc = parse('/', {'mode': 'LSD', 'sid1': category_id})

  items = []
  for li in doc.css('ul.r_news_normal li'):
    item = {}

    # title
    item['title'] = li.css('.r_news_tit strong::text').extract()[0]

    # url
    href = li.css('a.r_news_drw::attr("href")').extract()[0]
    item['url'] = ''.join(['http://m.news.naver.com', href])

    # image
    item['image'] = None
    if 0 < len(li.css('.r_news_im img')):
      item['image'] = li.css('.r_news_im img::attr("src")').extract()[0]
      # Remove resizing parameter
      item['image'] = item['image'].replace('?type=nf154_120', '')

    items.append(item)

  return items

def get_summary(url):
  doc = parse(url)

  # extract paragraphs
  paragraphs = [p.extract() for p in doc.css('#dic_area::text')]

  # sanitize paragraphs
  paragraphs = list(map(sanitize, paragraphs))

  # remove empty paragraphs
  paragraphs = [p for p in paragraphs if len(p) > 0]

  # first paragraph will be the summary
  summary = paragraphs[0]

  return summary

def sanitize(p):
  p = p.strip()

  # Recursively remove the parenthesized phrases that contain some rubbish
  # sentences or the names of journalist, photographer, and editor.
  changed = True
  while changed:
    p2 = re.sub(r'^\[([^\]]+)\]\s?', '', p)
    changed = (p != p2)
    p = p2

  # (REGION=JOURNAL) REPORTER =
  # 【REGION=JOURNAL】 REPORTER =
  p = re.sub(ur'^[\(【].+[\)】][^=]+= ', '', p)

  return p

def pretty(item):
  return u'''
category: {category}
title: {title}
url: {url}
image: {image}
summary: {summary}
'''.format(**item)

if __name__ == '__main__':
  for category in get_categories():
    for item in get_items(category['id']):
      item['category'] = category['name']
      item['summary'] = get_summary(item['url'])
      print pretty(item)
