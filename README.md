# How to run

```sh
easy_install scrapy
python spider.py
```

## Unit testing

```sh
python tests.py
```
